package co.simplon.promo18;

import co.simplon.promo18.bases.ExoMethods;

/**
 * Javadoc dans laquelle on va indiquer à quoi sert la classe en question
 * On peut aussi mettre de la javadoc sur les méthodes et les propriétés pour
 * indiquer ce qu'elles représentent
 *
 */
public class App {
    /**
     * Le public static void main est le point de départ
     * de l'application, on en a généralement qu'un seul par
     * appli. C'est lui qui va exécuter ce qui doit l'être.
     */
    public static void main(String[] args) {
        ExoMethods exoInstance = new ExoMethods();

        System.out.println(exoInstance.myName());

        String feedback = exoInstance.nameLengthFeedback("Le prénom à tester");
        System.out.println(feedback.toUpperCase());

        System.out.println(exoInstance.nameLengthFeedback("Jean"));

        

        System.out.println(exoInstance.addTwo(5));
        System.out.println(exoInstance.addTwo(10));
    }
}
