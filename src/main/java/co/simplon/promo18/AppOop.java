package co.simplon.promo18;

import co.simplon.promo18.oop.Dog;

public class AppOop {

    /**
     * Deuxième point d'entrée de l'application pour les petits exo poo
     * @param args
     */
    public static void main(String[] args) {
        Dog dog1 = new Dog("Fido", "Corgi", 5);
        Dog dog2 = new Dog("Rex", "Dalmatian", 10);
        Dog dog3 = new Dog("Albert", "Golden Retriever", 15);

        dog1.bark();
        dog3.bark();

        dog2.status();
        dog3.status();

        dog1.sniff(dog2);
    }
    
}
