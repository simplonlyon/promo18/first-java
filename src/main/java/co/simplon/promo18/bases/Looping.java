package co.simplon.promo18.bases;

public class Looping {

    /**
     * Une méthode qui va utiliser des boucles for imbriquées pour afficher
     * les tables de multiplication de 1 à 10. 
     * On aura donc une première boucle qui va de 1 à 10, et à chaque tour de
     * cette boucle là, on relance une nouvelle boucle de 1 à 10 également pour faire
     * la multiplication (on a donc en tout 100 tours de la boucle interne)
     */
    public void multiplicationTables() {
        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i*j + " ");
                
            }
            System.out.println();
        }
    }

    /**
     * Une méthode qui va afficher une pyramide d'étoiles en utilisant variables et
     * boucles imbriquées. (voir le README.md pour les différentes étapes du processus)
     */
    public void pyramid() {
        int starNumber = 1;
        int spaceNumber = 4;
        for (int i = 1; i <= 5; i++) {
            
            for (int j = 0; j < spaceNumber; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < starNumber; j++) {
                System.out.print("*");
            }
            System.out.println();
            

            starNumber += 2;
            // starNumber = starNumber + 2;
            spaceNumber--;
        }
        

        //Version alternative avec juste 2 boucles
        /*
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j < 5+i; j++) {
                if(j <= 5-i) {
                    System.out.print(" ");
                } else {
                    System.out.print("*");
                }
            }
            System.out.println();
            

            // starNumber = starNumber + 2;
        }
        */

    }
}
