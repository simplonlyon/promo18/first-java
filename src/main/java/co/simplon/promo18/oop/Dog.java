package co.simplon.promo18.oop;

public class Dog {
    public String name;
    public String breed;
    public int age;
    public int hunger = 50;

    public Dog(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    /**
     * Méthode qui va faire aboyer une instance de chien et augmenter sa faim de 10
     */
    public void bark() {
        System.out.println("woof woof");
        hunger+=10;
    }

    /**
     * Méthode qui va afficher le status d'une instance de chien, selon son age et sa faim
     */
    public void status() {
        System.out.println(name+" is happy because he dog");
        if(age > 12) {
            System.out.println(name+" is old");
        }
        if(hunger >= 60) {
            System.out.println(name+" want food");
        }
    }

    /**
     * Méthode qui va permettre à une instance de chien de renifler une autre instance
     * de chien
     * @param otherDog Le chien qui va être reniflé par l'instance qui déclenche la méthode
     */
    public void sniff(Dog otherDog) {
        System.out.println(name+" is sniffing "+otherDog.name+", it smells amazing");
    }
}
