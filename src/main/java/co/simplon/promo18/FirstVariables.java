package co.simplon.promo18;
/**
 * Une classe nous permet de regrouper des actions et variables ensembles
 * Par exemple, un Chien pourrait être une classe avec un nom, un age et une méthode lui permettant d'aboyer
 * Une classe devra être instanciée, c'est à dire créer un exemplaire
 * concret et utilisable de cette classe (voir dans le App.java)
 */
public class FirstVariables {
    /**
     * Ici on déclare une méthode dont le nom est examples, qui est public (utilisable
     * depuis n'importe où), et qui possède void comme type de retour, c'est à dire 
     * qu'elle ne renvoie aucune donnée dans on l'appel (contrairement au .length() sur
     * les chaînes de caractère par exemple qui lui renvoie du int)
     * Pour que le code contenu dans cette méthode soit exécuté, il faudra appeler la 
     * méthode depuis une instance de la classe FirstVariables (voir App.java)
     */
    @SuppressWarnings("all")//j'ai rajouté ça pour qu'il enlève les warning de variables non utilisées
    public void examples() {

        System.out.println("Coucou je suis dans la méthode example");
         // Nombres entiers
         byte firstByte = 127;
         short firstShort = 10000;
         int firstInt = 1000000000; // on utilise lui
         long firstLong = 1000000000000000000l;
 
         // nombres à virgule
         float firstFloat = 1.4f;
         double firstDouble = 1.4;
 
         // Les booléens : true ou false
         boolean firstBoolean = true;
         // les caractères, un seul caractère par variable, entre simple quotes
         char firstChar = 'b';
 
         String firstString = "Une chaîne de caractères";
 
         System.out.println(firstChar);
  
    }


    public void exercice() {
         
        int a = 5;
        int b = 2;
        System.out.println(a);
        System.out.println(b);

        int resultAdd = a+b;
        System.out.println(resultAdd);

        float resultDivide = (float)a/b;
        System.out.println(resultDivide);

        String firstName = "John";

        String name = "Johnson";

        String fullName = firstName+" "+name;

        System.out.println(fullName);

        int charCount = fullName.length();

        System.out.println(charCount);
    }

}
