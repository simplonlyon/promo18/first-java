package co.simplon.promo18.bases;

public class ExoMethods {
    
    /**
     * Une méthode qui va renvoyer un prénom (cela permettra d'utiliser
     * cette valeur de retour à l'extérieur, pour la modifier/l'afficher/etc.)
     * @return Le prénom renvoyé par la méthode
     */
    public String myName() {
        return "Prénom";
        
    }

    /**
     * Une méthode qui va renvoyer une valeur ou une autre selon la longueur du nom
     * qu'on lui fourni en argument
     * @param name Le nom dont on vérifie la valeur
     * @return Renvoie une phrase différente si le nom fait plus de 10 caractères ou non
     */
    public String nameLengthFeedback(String name) {
        if(name.length() > 10 ) {
            return "This name is quite long";
        }
        return "This name is not so long";
    }
    
    /**
     * Une méthode qui va attendre un nombre en argument et lui ajouté 2
     * @param number Le nombre auquel on va ajouter 2
     * @return Le résultat de l'addition
     */
    public int addTwo(int number) {
        return number+2;
    }

    /**
     * Méthode qui vérifie si un nombre est pair ou non
     * @param number Le nombre à vérifier
     * @return Renvoie true si le nombre est pair et sinon false
     */
    public boolean isEven(int number) {

        if(number % 2 == 0) {
            return true;
        }
        return false;
        
        //faire ça direct, ça revient au même
        //return number % 2 == 0;
    }

    /**
     * Méthode qui dit bonjour à un nom spécifique
     * @param firstName le nom à qui on veut dire bonjour
     */
    public void hello(String firstName) {
        System.out.println("Hello "+firstName);
    }

}
