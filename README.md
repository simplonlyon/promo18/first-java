

## Exercices

### Mini exo variables ([fichier](src/main/java/co/simplon/promo18/FirstVariables.java))
1. Dans le main, créer une variable a qui contiendra 5 et une variable b qui contiendra 2
2. Créer une variable qui contiendra le résultat de l'addition des deux premières variable
3. Créer encore une variable qui contiendra le résultat de la division de a par b
4. Créer une variable qui contiendra votre prénom, et une autre variable qui contient votre nom, puis faire en sorte de créer une variable fullName qui contiendra les deux ensemble (il va falloir chercher côté "concaténation")
5. Chercher comment afficher le nombre de caractères contenus dans la variable fullN

#### Externaliser du code dans une méthode
Dans la classe FirstVariables, créer une nouvelle méthode sur le modèle de la méthode examples, qui s'appellera exercice et qui contiendra le code de l'exercice fait précédemment (qui pour l'instant se trouve dans le main)

Faire en sorte d'appeler cette méthode exercice depuis le main (vous pouvez utiliser la même instance que celle que j'ai fait)

### tables de multiplication (pour travailler la boucle for classique, et imbriquée même)  ([fichier](src/main/java/co/simplon/promo18/bases/Looping.java))
1. Créer une nouvelle classe Looping, dans le package bases et dans cette classe, créer une méthode multiplicationTables() qui renverra rien
2. Dans cette méthode, faire une première boucle for classique qui affichera les nombres de 1 à 10 (on hésite pas à tester en faisant une instance de cette classe et en appelant la méthode dans le main)
3. Maintenant, on va faire exactement la même boucle, mais à l'intérieur de l'autre, et plutôt qu'afficher juste le nombre, de la boucle, on va l'afficher multiplier par celui de l'autre boucle
4. On fait en sorte de sauter une ligne à chaque tour de la première boucle et vendu

 Bonus : Faire une méthode avec argument qui affichera la table de multiplication pour 1 chiffre donnée (genre si je lui donne 2, ça affichera 2 4 6 8 10 12 14 16 18 20)


### Pyramide ([fichier](src/main/java/co/simplon/promo18/bases/Looping.java))
Le résultat final à afficher c'est ça par exemple : 
```
    *
   ***
  *****
 *******
*********
```
1. Créer dans la classe Looping, une autre méthoe pyramid() et faire en sorte de l'appeler dans le main
2. Première étape, faire une boucle qui va faire le bon nombre de tour, donc là on a 5 étages, donc on aimerait afficher ça via une boucle :
```
*
*
*
*
*
```
3. Maintenant qu'on a le bon nombre d'étage, on peut compter le nombre d'étoile qu'on a par étages, et essayer de trouver qu'est-ce qu'il se passe à chaque étage comme opération  (aide :||ya un +2 à chaque ligne||) pour pouvoir afficher ça par exemple 
```
1
3
5
7
9
```
4. Maintenant qu'on a l'opération du nombre d'étoile, on peut essayer d'afficher le nombre d'étoiles correspondantes à chaque étage (aide: ||on peut faire ça avec une boucle à l'intérieur de l'autre, comme on a fait à l'exercice d'avant par exemple||)
```
*
***
*****
*******
*********
```
5. On refait maintenant le même processus que le 3 et 4 mais cette fois pour le nombre d'espace qui apparemment passe de 4 à 3 à 2 à 1 à 0. Ce qui devrait nous donner, la pyramide

Bonus : Rajouter un argument à la méthode pyramid pour qu'on puisse choisir le nombre d'étages qu'on souhaite afficher
Bonus 2 : Trouver une manière pour que ça affiche les étoiles au fur et à mesure dans la console plutôt qu'afficher tout le résultat d'un coup

### Exo Méthodes ([fichier](src/main/java/co/simplon/promo18/bases/ExoMethods.java))
1. Créer une nouvelle classe dans le package bases, qui s'appelera ExoMethods (faire une instance de cette classe dans le main comme on a fait avec looping)
2. Créer une première méthode myName qui va renvoyer du String et qui fera un return de votre prénom
3. Créer une méthode nameLength qui va renvoyer du String également, qui va faire un if pour vérifier si la longueur de votre prénom est supérieur à 10 caractères, si oui faire un return "you have a long name", sinon ça va return "you don't have a long name" (bonus, on peut utiliser la méthode myName pour récupérer le prénom)
4. Créer une méthode addTwo qui va renvoyer du int et attendre un argument de type int et qui fera un return de l'argument plus 2
5. Créer une méthode isEven qui va renvoyer du booléen et attendre un int en argument et qui fera un return de false s'il est impair et renverra true s'il est pair
6. Créer une méthode skynetAI qui attendra une chaîne de caractères en paramètre et qui renverra "Hello how are you ?" si le paramètre est égal à "Hello", "I'm fine, thank you" si le paramètre est égal à "How are you ?", "Goodbye friend" si le paramètre est égal à "Goodbye" et renverra "I don't understand" pour n'importe quelle autre valeur
7. Créer une méthode getGreater qui attendra 2 int en argument et qui fera un return de l'int le plus grand des deux

### Exercice POO - Dog et propriétés ([fichier](src/main/java/co/simplon/promo18/oop/Dog.java))
Créer une classe Dog qui aura un name en String, une breed en String, un age en int
1. Créer la nouvelle classe Dog dans le package oop et définir ses propriétés
2. Générer un constructeur avec toutes les propriétés dedans (pour générer un constructeur, on met le curseur sur le nom de la classe, on clique sur la petite ampoule et on choisit generate constructor)
3. Faire 2 ou 3 instances de Dog dans le main et faire des sysout de chaque instance
4. Créer une méthode bark() dans Dog qui va faire un sysout de "woof"  puis appeler la méthode en question sur vos instances
5. Créer une méthode status() qui va faire un sysout du nom du chien, concaténé avec "is happy, because he dog"
6. Rajouter une propriété hunger en int, initialisée à 50 (pas la peine de rajouter de paramètre au constructeur pour ça)
7. Modifier la méthode bark pour qu'elle augmente la valeur de hunger de 10
8. Modifier la méthode status pour faire que si hunger est supérieur ou égal à 60, alors on affiche le name du chien concatené avec "want food"
9. Créer une méthode sniff qui va attendre un Dog en argument et qui va faire un sysout de name "is sniffing" le name du paramètre dog ("it smells amazing") 
