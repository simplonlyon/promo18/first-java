package co.simplon.promo18.oop;

/**
 * Une classe peut avoir des propriétés, c'est à dire des variables qui seront liées
 * à chaque instance de cette classe. Cela nous permettra de représenter des données
 * complexes plus facilement.
 * Ici par exemple on a un Student qui possède un nom, un prénom et une promo. Cela signifie
 * que chaque instance de la classe Student possèdera ses propres nom, prénom et promo qui seront
 * indépendants des instances des autres Student
 */
public class Student {
    public String firstName;
    public String name;
    public int promo;

    


    public Student(String firstName, String name, int promo) {
        this.firstName = firstName;
        this.name = name;
        this.promo = promo;
    }



    /**
     * La méthode greeting va utiliser les propriétés de l'instance qui a déclenché
     * cette méthode. (voir exemples dans le AppOop)
     */
    public void greeting() {
        System.out.println("Bonjour, je m'appelle "+firstName+" "+name);
    }


    
}
